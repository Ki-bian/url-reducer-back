const mongoose = require('mongoose');
const { mongo } = require('../configs/globalConfig');

mongoose.connect(`mongodb://${mongo.host}:${mongo.port}/${mongo.database}`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log('Connexion à MongoDB réussie !'))
  .catch(() => console.log('Connexion à MongoDB échouée !'));

module.exports = mongoose;
