const validUrl = require('valid-url');
const Urls = require('../models/Urls');
const shortCode = require('../middlewares/shortString');

exports.addOriginalUrl = async ({ body }, res) => {
  // controle the url
  if (validUrl.isUri(body.originalUrl)) {
    console.log('valide url format');
  } else {
    return res.status(404).json('Invalid Base Url format');
  }

  const createAt = new Date();
  const { originalUrl } = body;
  const { shortUrlBase } = body;

  if (validUrl.isUri(originalUrl)) {
    const urlCode = shortCode.generate();
    const urlReduced = `${body.shortUrlBase}/${urlCode}`;

    // Verify if the same url is in the model urls
    let isUrl = {};
    isUrl = await Urls.findOne({ shortUrlBase, originalUrl }).exec();

    if (isUrl) {
      return res.status(200).json(isUrl);
    }
    const url = {
      originalUrl,
      urlCode,
      shortUrlBase,
      urlReduced,
      createAt,
    };

    try {
      const urlSaved = new Urls(url);
      await urlSaved.save();
      return res.json(url);
    } catch (e) {
      return res.json({
        status: 1,
        message: "Une erreur s'est produite lors de l'ajout de l'url dans la base de données",
      });
    }
  } else {
    return res.status(404).json('Invalid Base Url format');
  }
};
