const mongoose = require('mongoose');

const urls = mongoose.Schema({
  originalUrl: { type: String, required: true },
  urlCode: { type: String, required: true },
  shortUrlBase: { type: String, required: true },
  urlReduced: { type: String, required: true },
  createAt: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Urls', urls);
