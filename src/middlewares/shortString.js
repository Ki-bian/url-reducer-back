const shortid = require('shortid');

exports.generate = () => shortid.generate();
