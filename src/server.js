const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const app = express();

require('../mongo/mongo');
const routes = require('./routes');

app.set('port', process.env.PORT || 5000);
app.use(bodyParser.json());
app.use(cors());

app.use('/api', routes);

const server = app.listen(app.get('port'), () => {
  console.log(`Express running → PORT ${server.address().port}`);
});
