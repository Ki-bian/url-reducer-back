const router = require('express').Router();

router.post('/urls', require('./controllers/urlController').addOriginalUrl);

module.exports = router;
