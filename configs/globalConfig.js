exports.server = {
  http: '5000',
  https: null,
  key: '',
  cert: '',
};

exports.client = {
  host: '/',
  http: 8081,
  https: true,
};

exports.smtp = {
  host: 'ssl0.ovh.net',
  port: 465,
  secure: true,
  auth: {
    user: '',
    pass: '',
  },
};

module.exports.mongo = {
  host: 'localhost',
  port: 27017,
  database: 'urlReducer',
};
